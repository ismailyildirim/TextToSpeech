package com.example.ismai.texttospeech;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

@RealmClass
public class Model extends RealmObject {
    String text;
    int id;

    public Model() {
    }

    public Model(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
