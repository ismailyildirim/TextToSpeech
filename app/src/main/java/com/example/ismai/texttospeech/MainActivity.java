package com.example.ismai.texttospeech;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements
        Adapter.MyCallBack, TextToSpeech.OnInitListener {
    List<Model> list;
    Adapter a;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView recyclerView;
    EditText text;
    Button add, remove;
    Adapter.MyCallBack myCallBack = this;
    TextToSpeech read;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Init();
        realm = Realm.getDefaultInstance();
        show();
    }

    public void Init() {
        list = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        add = findViewById(R.id.add);
        text = findViewById(R.id.text);

        Intent intent = new Intent();
        intent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(intent, 44);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();
                realmAdd(text.getText().toString());
                text.setText("");
            }
        });
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView); //set swipe to recylcerview
    }

    public void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void realmAdd(final String txt) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Model m = realm.createObject(Model.class);
                m.setText(txt);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                show();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
            }
        });
    }

    public void show() {
        RealmResults<Model> words = realm.where(Model.class).findAll();
        a = new Adapter(getApplicationContext(), words, myCallBack);
        recyclerView.setAdapter(a);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 44) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                read = new TextToSpeech(this, (TextToSpeech.OnInitListener) this);
             /*   Locale locale = new Locale("tr", "TR");
                int result = read.setLanguage(locale);*/
            } else {
                Intent intent2 = new Intent();
                intent2.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
                startActivity(intent2);
            }
        }
    }

    @Override
    public void listenerMethod(String textViewvalue) {
        if (textViewvalue != null && textViewvalue.length() > 0) {
            read.speak(textViewvalue, TextToSpeech.QUEUE_ADD, null);

        }
    }

    @Override
    public void onInit(final int status) {

    }

    @Override
    public void listenerGetPosition(final int position) {
        final RealmResults<Model> list = realm.where(Model.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Model m = list.get(position);
                m.deleteFromRealm();
                show();
            }
        });
    }

    //KAYDIRMA İŞLEMİ
    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
            final int position = viewHolder.getAdapterPosition(); //get position which is swipe

            if (direction == ItemTouchHelper.LEFT) {    //if swipe left

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this); //alert for confirm to delete
                builder.setMessage("Are you sure to delete?");    //set message

                builder.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() { //when click on DELETE
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final RealmResults<Model> list = realm.where(Model.class).findAll();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                Model m = list.get(position);
                                m.deleteFromRealm();
                                show();
                            }
                        });
                        return;
                    }
                }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {  //not removing items if cancel is done
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        show();
                    }
                }).show();
            }
        }
    };


}
