package com.example.ismai.texttospeech;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.define> {
    Context context;
    List<Model> list;
    MyCallBack myCallback;

    public interface MyCallBack {
        void listenerMethod(String textViewvalue);

        void listenerGetPosition(int position);
    }

    public Adapter(Context context, List<Model> list, MyCallBack myCallBack) {
        this.context = context;
        this.list = list;
        this.myCallback = myCallBack;

    }

    @NonNull
    @Override
    public define onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout, parent, false);
        return new define(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final define holder, final int position) {
        holder.text.setText(list.get(position).getText());
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCallback.listenerGetPosition(position);
                notifyDataSetChanged();

            }
        });
        holder.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCallback.listenerMethod(holder.text.getText().toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class define extends RecyclerView.ViewHolder {
        TextView text;
        Button remove;

        public define(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.layoutText);
            remove = itemView.findViewById(R.id.remove);
        }
    }
}
